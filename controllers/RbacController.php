<?php

/**
 * Created by PhpStorm.
 * User: a_djuraev
 * Date: 15.06.2016
 * Time: 15:17
 */

namespace juraev\rabc\controllers;


use yii\web\Controller;

class RbacController extends Controller
{

    public function actionInit()
    {
        $authManager = \Yii::$app->authManager;

        // Create roles
        $guest      = $authManager->createRole('guest');
        $member     = $authManager->createRole('member');
        $editor     = $authManager->createRole('editor');
        $admin      = $authManager->createRole('admin');
        $superAdmin = $authManager->createRole('superAdmin');

        // Create simple, based on action{$NAME} permissions
        $p_guest        = $authManager->createPermission('guest');
        $p_member       = $authManager->createPermission('member');
        $p_editor       = $authManager->createPermission('editor');
        $p_admin        = $authManager->createPermission('admin');
        $p_superAdmin   = $authManager->createPermission('superAdmin');

        // Add permissions in Yii::$app->authManager
        $authManager->add($p_guest);
        $authManager->add($p_member);
        $authManager->add($p_editor);
        $authManager->add($p_admin);
        $authManager->add($p_superAdmin);

        $authManager->addChild($guest, $p_guest);
        $authManager->addChild($member, $p_member);
        $authManager->addChild($editor, $p_editor);
        $authManager->addChild($admin, $p_admin);
        $authManager->addChild($superAdmin, $p_superAdmin);

        $authManager->addChild($admin, $member);
        $authManager->addChild($admin, $editor);
        $authManager->addChild($superAdmin, $admin);

    }

}