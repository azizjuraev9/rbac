<?php
/**
 * Created by PhpStorm.
 * User: a_djuraev
 * Date: 15.06.2016
 * Time: 13:01
 */

namespace juraev\rabc;


use Yii;

class Rbac extends \yii\base\Module
{

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'juraev\rabc\controllers';
    public $defaultRoute = 'menu/new';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'juraev\rabc\commands';
        }
        // custom initialization code goes here
    }
}
