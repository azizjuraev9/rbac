Juraev RBAC
===========
Juraev RBAC

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist juraev-rbac/juraev-rbac "*"
```

or add

```
"juraev-rbac/juraev-rbac": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \\juraev\rabc\AutoloadExample::widget(); ?>```